using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGame : MonoBehaviour
{
    private bool hasCollisioned = false;

    public GameObject SaveText;
    public GameObject SavedText;
    public GameObject SaveButtons;

    // Start is called before the first frame update
    void Start()
    {
        SavedText.SetActive(false);
        SaveText.SetActive(false);
        SaveButtons.SetActive(false);
    }

    public void Save() {

        SaveText.SetActive(false);
        SavedText.SetActive(true);
        SaveButtons.SetActive(false);

        Invoke("DisableText", 4.0f);

    }

    public void DisableText() {
        SavedText.SetActive(false);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Pencil" && !hasCollisioned)
        {

            SaveText.SetActive(true);
            SaveButtons.SetActive(true);
            hasCollisioned = true;
        }

      
    }
}
