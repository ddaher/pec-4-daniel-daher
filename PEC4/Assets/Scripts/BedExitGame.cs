using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BedExitGame : MonoBehaviour
{
    public Animator animator;
    public GameObject tpObject;
    public GameObject xrController;

    public void GoToBed() {

        xrController.transform.position = tpObject.transform.position;
        xrController.transform.rotation = tpObject.transform.rotation;
        animator.SetBool("GoToBed", true);
    }
}
