using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioMusicVolume : MonoBehaviour
{
    public AudioSource BackgroundMusic;
    public float VolumeValue;

    // Start is called before the first frame update
    void Start()
    {
        VolumeValue = BackgroundMusic.volume;
    }

    public void VolumeUp() {
        VolumeValue += 0.1f;

        if (VolumeValue < 1f)
        {
            BackgroundMusic.volume = VolumeValue;
        }
        else
        {
            BackgroundMusic.volume = 1f;
        }

    }

    public void VolumeDown()
    {
        VolumeValue -= 0.1f;

        if (VolumeValue > 0f)
        {
            BackgroundMusic.volume = VolumeValue;
        }
        else
        {
            BackgroundMusic.volume = 0f;
        }

    }
}
