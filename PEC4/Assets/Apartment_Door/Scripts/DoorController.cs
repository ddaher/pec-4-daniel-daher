﻿using UnityEngine;
using UnityEngine.UI;

public class DoorController : MonoBehaviour
{
    public Animation doorAnim;
    public GameObject DoorHandleButton;

    public void OpenDoor() {

        DoorHandleButton.SetActive(false);
        doorAnim.Play("Door_Open");
    }
}
